# Overview

This section aims to provide a high-level introduction to the C2MON platform.
After reading this chapter, you should have a good idea about how data flows through C2MON; from the raw data coming from sensory data sources, all the way up to user applications that make use of that data.

To begin with, we will describe the overall architecture of the platform.
Then, in later sections, we will dive deeper into each layer and understand the core concepts that are needed to work with the system.


## What is C2MON?

The Controls and Monitoring Platform C2MON is a toolkit written in Java for building highly complex, distributed and fail-safe monitoring solutions.
Therefore, it uses a 3-tier architecture with the possibility to run multiple servers at the same time, which allows applying patches and updates without affecting the service availability.
C2MON manages centrally the configuration of all running Data Acquisition (DAQ) Processes and handles their reconfiguration online without any downtime or potential data loss.
The modular architecture builds on a core system that aims to be reusable for multiple monitoring scenarios, while keeping each instance as lightweight as possible.

C2MON comes with a modern looking web interface that provides many core functionalities for data browsing, administration and analysis.



### Use Cases

Since C2MON is essentially a heterogeneous data acquisition framework with configuration, persistence, historical browsing, control and alarm functionalities, it can be suitable for building many different types of system.

For example, it is used internally at CERN as an industrial SCADA system; as a network monitoring system; as a central alarm aggregation service; and as a general-purpose data proxy.

Reuse for instance C2MON within your project as open SCADA middleware whilst focusing on client application development.
Some more examples are listed below.

C2MON can be used:

* To acquire and store data from different type of systems,
* To build up a simple or highly distributed or cloud based monitoring and control solution,
* To realise high-availability solutions with on-line reconfiguration,
* To share data from hundred of thousands or even millions of data sensors with multiple types of applications,
* To cluster data acquisition with event notifications and to execute background tasks (e.g. rule or alarm evaluations),
* To centrally manage the subscription configuration from different type of systems,
* To define structured objects (Devices) on top of your data acquisition that can be re-used on the client tier,
* As data analysis framework,
* As simple data proxy,
* As filtering system to reduce for instance the noise of analogue sensors,
* As data recorder,
* As data history player for client applications to replay for instance highly complex synoptic dashboards.


### Architectural Overview

The C2MON platform uses a 3-tier architecture, as displayed in the diagram below.

![Screenshot](images/overview/C2MON-Design-Overview.png =500x)

**Data Acquisition (DAQ) Layer**

The data acquisition (DAQ) layer provides solutions for acquiring data from a number of protocols/hardware.
For detailed documentation about the DAQ layer, see C2MON Data Acquisition.

**Server Layer**

The server architecture is designed around a (distributed) cache, which keeps for each configured sensor the latest value In-Memory.
Internally, the server is broken down into a number of modules, including the possibility to write optional modules providing extra functionalities.
The technology stack is based on Java Spring container and is designed to run in a clustered setup.

**Client Layer**

Communication with the client layer is done via a provided C2MON Client API, which is documented here: [Client API](client_API.md).


<a id="_core_concepts"></a>
## Core Concepts

This section aims to provide a general introduction into the core concepts of the C2MON platform.


### Process/Equipment

This page aims to provide a general introduction into the Process/Equipment data configuration concept within C2MON.
Processes & Equipments are used to structure the data points when creating monitoring configurations via the C2MON Configuration Loader.
This concept forms the structural configuration and supervision base and is indispensable for every setup.


#### Process/Equipment Structure

The Process/Equipment structure is conceptually simple:

* A **Process** represents a single Data Acquisition Process (DAQ).
* An **Equipment** represents a piece of physical sensory hardware, or any other kind of data source such as a middleware service or database. A Process can have one or more Equipments.
* A **Sub-Equipment** represents a logically separate data source within an Equipment. An Equipment can optionally contain one or more SubEquipments.

The following diagram shows an abstract view of the concept:

![Screenshot](images/overview/process-equipment.png =300x)

These three structures provide the flexibility within C2MON to model and monitor a diverse range of data acquisition needs. The following sections describe them in more detail.

**Process**

The Process is a representation of a single Data Acquisition Process (DAQ), which is the actual Java process running on a machine and which communicates with the data source.
It has two main functions:

* **Monitoring the connection between the DAQ and server.** This is done via StatusTags, AliveTags and CommFaultTags (Tags and health monitoring are explained in Core Concept: Tags and Core Concept: Supervision).
* **Performing management operations on a DAQ** (such as starting, stopping and restarting it remotely) via CommandTags (Tags are explained in Core Concept: Tags)

**Equipment**

The Equipment represents the actual data source which sends data to the DAQ process. The Equipment has a number of DataTags, which correspond to data points being sent from the data source.

The Equipment is also used to monitor the status of the connection between the DAQ process and the data source, again via StatusTags, AliveTags and CommFaultTags.

**Sub-Equipment**

The SubEquipment is attached to a given Equipment and represents a sub-system of that Equipment.
The SubEquipment also has a number of DataTags corresponding to data points being sent from the data source.

The connection between the SubEquipment and its parent Equipment is also monitored via StatusTags, AliveTags and CommFaultTags.

<a id="_configuration_structure"></a>
#### Configuration structure

The data structure of a DAQ configuration is strictly hierarchical.
The smallest configurable unit are tags, which are either attached an Equipment or Sub-Equipment.

![Screenshot](images/overview/process-data-relation.png =500x)

The tags in dark blue are mandatory tags for the Supervision and have to be generated together with every new Process, Equipment or Sub-Equipment (see also Core Concept: Supervision).






<a id="_tags"></a>
### Tags

Almost all information in C2MON is stored and flows through the system in the form of Tags.
In this section, we describe what Tags are and what their role is in the C2MON architecture.


#### The Tag interface

The Tag interface embodies the common structure of 3 data types in the C2MON architecture: the **DataTag**, the **ControlTag** and the **RuleTag**.
Tags should be thought of as follows: objects implementing this interface are destined to be updated, logged, published to clients and used in data views.
They will be able to use all the provided infrastructure of the C2MON architecture, including all the functionalities of the Client API.

Every Tag includes a **quality** object that indicates whether the received value is trustable or not.
The Tag quality is hence either ok (valid) or bad (invalid).
The cause for a Tag being marked as invalid can have multiple reasons which is further discussed [below](#_tag_quality).

Recall that each Equipment/Subequipment can have a set of associated Tags.
On the other hand, Tags always belong to one and only one Equipment/SubEquipment.

Tags can also have Alarms attached. to understand Alarms, please read the section about [Alarms](#_alarms).


#### The DataTag

The DataTag is a Tag coming from an external data source.
It corresponds to a single external data point, represented by a Java primitive type (String, Float, Integer, etc.).
It is attached to an Equipment, which embodies the type of data source it originates from.

Every DataTag has to provide a so-called "hardware address", which contains the information needed for subscribing to this data point.
C2MON leaves it completely free how that hardware address is formatted.
More information about that topic is provided in [Creating a new DAQ module from scratch](guides/DAQ_module_developer_guide.md#_creating_a_new_daq_module_from_scratch).


> **Please note!**

>**DataTag value type**

>Many functionalities in the C2MON server are only available for data points with primitive type values, such as Floats or Strings. However, the core C2MON could handle more complex data type without too many changes. DataTag values are stored as simple Java Objects.


#### The RuleTag

The RuleTag is a Tag internal to C2MON built on top of other Tags.
Its value is specified by a Rule expression given in a specific C2MON rule grammar.
This expression specifies how the Rule value is derived from the value of the Tags that feed into it.
Changes to these Tags will result in an update of the Rule, based on the Rule expression.

> **Please note!**

>For more information about how to create rule expressions for RuleTags, please read the chapter about the [Rules Engine](rules.md#_rules_engine).


#### The ControlTag

The ControlTag is any Tag used by the C2MON system for monitoring/publishing internal states of the system itself (we include here Equipment as part of the system, even if it may be shared with others). In other words, ControlTags are used by C2MON for self-monitoring of some component of the system. Making use of the functionalities available to all Tags, these values can then be logged and sent to C2MON clients for self-monitoring purposes.
Alarms may also be attached to detect critical problems.

There are three types of ControlTag:

* The StatusTag;
* The AliveTag;
* The CommFaultTag.

For a detailed description of these three tags and the role they play in monitoring the status of Processes and Equipments, read the section about [Supervision](#Supervision).

Equipment AliveTags are a good example of a ControlTag, since they are used to monitor the status of a supervised Equipment.
In a similar way, an Equipment StatusTag is used to publish the current status (running or down) of an Equipment.


#### The CommandTag

The CommandTag is a special type of Tag that is used to send commands from the client layer to the DAQ layer.
It is the only Tag that travels in this direction; all other Tags travel from the DAQ layer to the client layer.

CommandTags are usually used to send start/stop/restart commands to a DAQ process.

<a id="_tag_quality"></a>
#### Tag Quality 

The Tag quality is a fundamental concept of C2MON, used to indicate the health state of the sensor communication.
In case of communication problems between the source and C2MON the Tag quality is giving very precise information about what went wrong (see table below).

Another reason why the Tag quality is absolutely needed comes from the fact that C2MON is filtering out all redundant value updates coming from the source.
But many systems do exactly that and keep on sending over and over again in regular intervals their data like heartbeats.
C2MON replaces this by the Equipment and Sub-Equipment heartbeat (see also [Supervision](#Supervision)).

A Tag update notification can hence be triggered by a change of the quality state.
It is therefore absolutely necessary to always check the validity of the received Tag update.

> **Please note!**

>A tag can accumulate multiple invalidation state.
>A subscribed client will be notified at every change, independent if an invalidation quality state is added or removed.

The following table lists all quality flags that a Tag can receive.

| Quality |Description |
--------- | ------------
| OK | This is the standard quality flag indicating that the value is VALID and no error occured during data transmition. |
| UNDEFINED_TAG | A tag with this identifier/name is not known to the system. |
| JMS_CONNECTION_DOWN | The Client API lost the connection to the JMS broker. |
| SERVER_HEARTBEAT_EXPIRED | The Client API did not receive a valid server heartbeat. |
| PROCESS_DOWN | The server has marked the **DAQ** process responsible for the given Tag's data acquisition as down. This is for instance also the case when the DAQ gets restarted. This flag will be removed from all tags once the DAQ is detected as running again. |
| EQUIPMENT_DOWN | All Tag's from a given Equipment (or service) will receive this invalidation flag, in case that the communication to the underlying device got interrupted. |
| SUBEQUIPMENT_DOWN | Tag's being assigned to a Sub-Equipment will receive this quality flag e.g. in case the given sub-equipment heartbeat expires. |
| UNDEFINED_VALUE | A Tag's value cannot be determined. This is normally a value cast problem, since C2MON will always try to cast a received value to the configured value type. |
| VALUE_OUT_OF_BOUNDS | The Tag configuration allows defining value ranges. If your tag is for instance representing a percentage (%) value, it should always stay between 0 and 100. So, in case the value is not inside the defined boundaries the Tag will be flagged as INVALID and the quality object will indicate this quality code. |
| INACCESSIBLE | The server will mark all Tags as inaccesible, for which it never has received an initial value. |
| UNKNOWN_REASON | Reason for invalidity could not clearly be identified |


<a id="Supervision"></a>
### Supervision

This section describes the mechanism by which C2MON is able to monitor (or supervise) parts of the system.
To supervise a part of the system is to maintain information about its health, and to react when problems are detected with it.

As mentioned in Core Concept: Tags, C2MON provides three types of ControlTag (the StatusTag, the AliveTag and the CommFaultTag).
These are the tags which represent the health state of an entity within the system.


#### The AliveTag

The AliveTag acts like a heartbeat for a particular entity.
If the AliveTag is not received regularly (within a configurable time interval) then C2MON assumes that the entity is not running and that there is a major problem.

#### The CommFaultTag

The CommFaultTag can be sent at any time, and indicates that there is some kind of communication problem with the entity.
The CommFaultTag has a Boolean value; true indicates that there is no problem, false indicates a problem.

> **Please note!**

>The false alarm behavior concept comes from controlling logic, were an alarm is raised if the current (1) gets interrupted (0).

#### The StatusTag

The StatusTag represents the overall health of the supervised entity, and is derived from the AliveTag and the StatusTag. If either the AliveTag expires or the CommFaultTag indicates a problem, then the StatusTag is set accordingly to a String value representing its state. There are several values that the StatusTag can have:

* **RUNNING** indicates that the entity is running normally.
* **DOWN** indicates that the entity is not running, either due to an expired AliveTag or a received CommFaultTag.
* **STARTUP** indicates that the entity is starting up.
* **STOPPED** indicates that the entity was shut down cleanly.
* **UNCERTAIN** indicates the server is not sure of the status of the entity, for instance after a period of server downtime.

The following diagram shows how the AliveTag and CommFaultTag influence the StatusTag:

![Screenshot](images/overview/supervision-tags.png =500x)

#### Supervisable Entities

The entities that are supervisable within C2MON are the Process, the Equipment and the SubEquipment (which were introduced in Core Concept: Process/Equipment).
This means that Processes, Equipments and SubEquipments can all be independently supervised with their own ControlTags.

**Supervision Flow: AliveTags**

The following diagram shows how the supervision flow for an AliveTag works.
As you can see, the AliveTag is checked periodically for expiration.
If it has expired, it triggers a change in the StatusTag and the CommFaultTag.
If it is received normally, it also triggers a reset of the StatusTag and CommFaultTag back to normal.

![Screenshot](images/overview/alivetag-flow.png)


**Supervision Flow: CommFaultTags**

The following diagram shows how the supervision flow for a CommFaultTag works.
When a CommFaultTag is received, its value is checked. If the value is false (which indicates a problem) then the StatusTag is changed to DOWN and the AliveTag is invalidated. If the value is true (which indicates no problem) then the StatusTag is changed to RUNNING and the AliveTag becomes valid.

![Screenshot](images/overview/commfaulttag-flow.png)



<a id="_alarms"></a>
### Alarms

Tags change frequently within C2MON as data from DAQ processes are received and evaluated by the system.
Sometimes, your sensory equipment may produce values that are worrying, dangerous or incorrect (for whatever reason). C2MON provides a mechanism to alert you when this happens. This page describes the Alarm mechanism.


#### The Alarm

In essence, an Alarm is a declaration associated with a Tag that contains some kind of condition specifying the legal values for the Tag. When the Tag value changes, the system will check the Alarm condition. If the new value is outside the legal value range, then the Alarm will be activated and pushed to the client. The main characteristics are:

* An alarm is ALWAYS attached to one and only one Tag, which can be either a RuleTag, ControlTag or DataTag.
* A Tag can be attached to many different alarms
* Alarms are defined by a XML condition in the database


#### Alarm Conditions

C2MON supports two Alarm conditions out-of-the-box. These conditions are:

* **ValueCondition:** if the Tag value equals a specific value, the alarm is triggered.
* **RangeCondition:** if the Tag value falls outside the given range, the alarm is triggered.

> **Please note!**

>It is possible to define custom Alarm condition types, but that is out of scope of this section.

The diagram below shows an example how Alarm condition can be assigned to results of rules or tags.

![Screenshot](images/overview/alarm-evaluation-example.png =500x)




### Class/Device/Property

This concept provides a different, more object-oriented way of structuring monitoring data coming from C2MON DAQs.

The Process/Equipment structure is the original concept within C2MON, and has existed from the very beginning.
The Class/Device/Property structure is much newer, and was added as an alternative to the Process/Equipment structure.

In fact, the Class/Device/Property structure cuts across the Process/Equipment structure.
It is simply an abstraction layer provided by the server to client applications.

> **Please note!**

>This section describes C2MON data configuration only from a conceptual point of view.
>To learn how to actually configure monitoring data, please read Server data configuration and Configuration module.

>To learn how to access data in Class/Device/Property format from the C2MON Client API, please see Client API: Device/Class/Property Extension.


#### Class/Device/Property Structure

The Class/Device/Property is conceptually simple to understand:

* A **Device Class** is a template for a particular type of Device to be monitored. A Class has a number of Property templates associated to it.
* A **Device** is a concrete instance of a Device Class, and has concrete instances of the Properties defined in its parent Device Class.
* A **Property** is a data point from the monitored Device. Usually, the property is a DataTag.

The following diagram shows a Device Class with three concrete Devices. Note that the Device does not have to provide instances for all properties.

![Screenshot](images/overview/class-device-property.png)


#### Constraints

* A Device can only belong to a single Device Class (multiple inheritance not supported).
* A Property belongs to only one Device.
