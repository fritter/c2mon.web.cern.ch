# C2MON User Manual

Welcome to the C2MON User Manual.
This manual includes concepts, instructions and samples to guide you on how to use C2MON and build C2MON monitoring solutions.

As the reader of this manual, you must be familiar with the Java programming language and you should have installed your preferred IDE.

> Please Note!
> - This documentation is currently under construction, since we are migrating it from an internal Wiki page.
> - You find further documentation in the following PDF: [C2MON-manual.pdf](https://edms.cern.ch/file/1221592/4/C2MON-manual.pdf)
> - If you are still missing some information, please let us know!


## Product Naming

*C2MON* (C-2-MON) stands for 'CERN Control and Monitoring Platform'

## Customer Support

We provide best effort support via <c2mon-support@cern.ch>
