## C2MON Configuration Guide

Before starting reading this guide please make sure that you are at least familiar with the following sections:

* [Core Concepts](../overview.md#_core_concepts)
* [Database structure](../appendices/database_structure.md)
* [Creating a new DAQ module from scratch](DAQ_module_developer_guide.md)

At the end of this guide you should be able to configure Processes, Equipments and Tags.


### Introduction

C2MON centrally manages the configuration of all Data Acquisition (DAQ) modules.
A DAQ receives its configuration at start-up, which has two main advantages:

* A DAQ needs only to know its unique name to request the configuration.
  No local configuration file or database access is required.
  This makes the deployment very light-weight and simplifies the setup of a cold standby.
* Since the server is orchestrating all DAQs it should never receive updates for unknown Tags.

An intuitive Java configuration API allows creating or updating Processes, Equipments and Tags.
If the DAQ module supports live configuration, the changes are automatically propagated.

<a id="_online_config_with_c2mon_config_api"></a>

### Online configuration with the C2MON Configuration API

To use the Configuration API with your C2MON cluster you have first to follow the <<Setup>> instructions of the <<Client API>>.

The configuration API allows to *create*, *update* or *remove* running DAQ entities.

The following DAQ entities are configurable from the configuration module:

* Process
* Equipment
* SubEquipment
* DataTag
* RuleTag
* Alarm

#### Structure of a configuration

In Order to send a configuration to the Server you have to get a ConfigurationService instantiation.
The ConfigurationService provides two ways to send a configuration.
 
* Using the __standard__ configuration methods which only needs mandatory arguments for creating and updating entities.

* Calling the __enhanced__ methods which expect a name and a _ConfigurationEntity_ that contains more specific information.  

#### Standard configuration

With the standard configuration ypu can create entities with only mandatory parameters.
If you like to specify your entities like adding a _description_ you have to use the <<Enhanced configuration>>.

>All parameters of the upcoming methods are mandatory and cant replaced with `null`.

##### Creating a Process

The following code shows an example of how to create a `Process`:

```java
ConfigurationService configurationService = C2monServiceGateway.getConfigurationService();
ConfigurationReport report = configurationService.createProcess("P_EXAMPLE");
```

Notes: 

* The process id will be generated automatically on the server and is listed in the report.
* The `AliveTag` and `StatusTag` tags will be generated automatically.

##### Creating an Equipment

The following code shows an example of how to create a `Equipment`:

```java
ConfigurationService configurationService = C2monServiceGateway.getConfigurationService();
ConfigurationReport report = configurationService.createEquipment("P_EXAMPLE", "E_EXAMPLE", "c2mon.driver.demo.MyEquipmentMessageHandlerImpl");
```

Notes: 

* The equipment id will be generated automatically on the server and is listed in the report.
* The `CommFaultTag` and `StatusTag` tags will be generated automatically.
* The first parameter expects the name of an already created `Process`.

##### Creating a DataTag

The following code shows an example of how to create a `DataTag`:

```java
ConfigurationService configurationService = C2monServiceGateway.getConfigurationService();
ConfigurationReport report = configurationService.createDataTag("E_EXAMPLE", "DataTag_EXAMPLE", Integer.class, new DataTagAdress();
```

Notes: 

* The tag id will be generated automatically on the server and is listed in the report.
* The first parameter expects the name of an already created `Equipment`.

##### Creating an Alarm

The following code shows an example of how to create a `Alarm`:

```java
ConfigurationService configurationService = C2monServiceGateway.getConfigurationService();
ConfigurationReport report = createAlarm("DataTag_EXAMPLE", new ValueCondition(Integer.class, 1), "faultFamily", "faultMember", 0);
```

Notes: 

* The alarm id will be generated automatically on the server and is listed in the report.
* The first parameter expects the name of an already created `Tag`.

##### Creating a RuleTag

The following code shows an example of how to create a `RuleTag`:

```java
ConfigurationService configurationService = C2monServiceGateway.getConfigurationService();
ConfigurationReport report = createRuleTag("RuleTag_EXAMPLE", "(#1000 < 0)|(#1000 > 200)[1],true[0]", Integer.class);
```

##### Deleting a Entity

The following code shows a general example of how to delete a Entity:

```java
ConfigurationService configurationService = C2monServiceGateway.getConfigurationService();
ConfigurationReport report = removeProcess("P_EXAMPLE");
```

Notes: 

* Alternatively you can use `removeProcessById()`. 
* Every entity has a similar method for deleting.

#### Enhanced configuration

The enhanced configuration is the only way to specify all information for every entity.
It is based on the builder pattern which comes along with a `create()` and `update()` method for each entity.
After creating objects with the builder methods you have to pass them to the corresponding configuration methods in the _ConfigurationService_ interface.

Notes: 

* The builder methods expects mandatory arguments for the corresponding action.
* After invoking the builder method all available methods of the builder shows the optional fields you can set.

##### Creating and updating a Entity

If you like to __create__ or __update__ an entity with more specific information you have to use the _configuration POJOs_ 
which have to be created through associated builder methods.
For each (creating and updating) you can pass then the POJO to a corresponding method of the `ConfigurationService` as shown below. 
 

###### Create a Equipment
```java
Equipment equipmentToCreate = Equipment.create("E_EXAMPLE", "c2mon.driver.demo.MyEquipmentMessageHandlerImpl")
        .id(123L)
        .description("A short description of the Process")
        .aliveTag(AliveTag.create("").build(), 70000)
        .build();
        
ConfigurationService configurationService = C2monServiceGateway.getConfigurationService();
ConfigurationReport report = createEquipment("P_EXAMPLE", processToCreate);
```

Notes: 

* You must use the static create method to create a Equipment with additional information.
* Accidentally using the static update method will throw an exception.
* All builder methods of the CreateBuilder indicates the fields you optional set.
* If you like to specify your own `AliveTag` you have to use the create builder method of the `AliveTag` as well.
* If you like to specify your own `AliveTag` you must also set the alive time in the same method call.

###### Update a Equipment
```java
Equipment equipmentToUpdate = Equipment.update("E_EXAMPLE")
        .description("A update description of the Process")
        .handlerClass("c2mon.driver.demo.MyOtherEquipmentMessageHandlerImpl")
        .aliveInterval(80000)
        .build();
        
ConfigurationService configurationService = C2monServiceGateway.getConfigurationService();
ConfigurationReport report = updateEquipment(equipmentToUpdate);
```

Notes: 

* You must use the static update method to update a Equipment.
* Accidentally using the static create method will throw an exception.
* All builder methods of the UpdateBuilder indicates the fields you can update.

