# How-to compile the C2MON Documentation?

The C2MON documentation is written in [AsciiDoc](http://www.methods.co.nz/asciidoc/).

To generate the full documentation we use
[Asciidoctor](http://asciidoctor.org/), so please make sure you have it installed before executing the following command:

```bash
> cd c2mon-doc
> asciidoctor ./index.adoc
```

As result you should get an _index.html_ file containing the full documentation. Please notice, that you have to deploy it together with the css/ folder
