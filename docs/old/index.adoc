:homepage: http://cern.ch/c2mon
:doctype: book
:toc: left
:icons: font
:source-highlighter: highlightjs
:listing-caption: Listing
:numbered:
:linkcss:
:stylesheet: css/bootstrap_cerulean.css

include::header.adoc[]

include::preface.adoc[]

include::overview.adoc[]

include::client_API.adoc[]

include::rules.adoc[]

include::guides.adoc[]

include::appendices.adoc[]
